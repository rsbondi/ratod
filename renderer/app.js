import "./components/library.js";
import "./components/reader.js";
const { ipcRenderer } = require('electron')
import { addClass, removeClass } from './dom.js'

const library = document.querySelector('#lib')
const reader = document.querySelector('#read')

ipcRenderer.on('current-book-set', (e, arg) => {
	if (arg.id === -1) {
		removeClass(library, 'collapsed')
		addClass(reader, 'collapsed')
	} else {
		addClass(library, 'collapsed')
		removeClass(reader, 'collapsed')
	}
})

export default {}
