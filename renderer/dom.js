/**
 * Create a dom element
 * @param {string} cls - a class name for identification
 * @param {string} tag - type of element, defaults to div if ommited
 * @returns {HTMLElement}
 */
export function el(cls, tag) {
	const el = document.createElement(tag || 'div')
	if (cls) el.className = cls
	return el
}

/**
 * Helper function for adding event listener
 * @param {HTMLElement} el - element to add listener to
 * @param {stromg} event - the name of the event
 * @param {Function} handler - event handler function
 */
export function listen(el, event, handler) {
	el.addEventListener(event, handler)
	return () => el.removeEventListener(event, handler)
}

/**
 * Helper function to add a class to an element
 * @param {HTMLElement} el - the elemet to add class to
 * @param {string} name - class name
 */
export function addClass(el, name) {
	const cn = el.className.split(' ')
	if (!~cn.indexOf(name)) cn.push(name)
	el.className = cn.join(' ')
}

/**
 * Helper function to remove a class to an element
 * @param {HTMLElement} el - the elemet to remove class to
 * @param {string} name - class name
 */
export function removeClass(el, name) {
	const cn = el.className.split(' ')
	if (~cn.indexOf(name)) cn.splice(cn.indexOf(name), 1)
	el.className = cn.join(' ')
}

/**
 * Enable a button
 * @param {HTMLButtonElement} button - the button to enable
 */
export function enableButton(button) {
	button.removeAttribute('disabled')
	removeClass(button, "disabled")
}

/**
 * Disable a button
 * @param {HTMLButtonElement} button - the button to disable
 */
export function disableButton(button) {
	button.setAttribute('disabled', 'disabled')
	addClass(button, 'disabled')
}

/**
 * Helper function to remove a class to an element
 * @param {HTMLElement} el - the elemet to remove class to
 * @param {string} add - add class name
 * @param {string} remove - remove class name
 */
export function replaceClass(el, add, remove) {
	addClass(el, add)
	removeClass(el, remove)
}
