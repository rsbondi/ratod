import Collapsable from './base/collapsable.js'
import { el, listen } from '../dom.js'

class UrlDisplay extends Collapsable {
	constructor(container) {
		super(container)
	}

	mount() {
		this.root = el('urldisplay-content')
		const frag = document.createDocumentFragment()
		const closex = el('close')
		closex.textContent = 'x'
		listen(closex, 'click', this.hide.bind(this))
		frag.appendChild(closex)

		this.urldisplay = el()
		frag.appendChild(this.urldisplay)

		this.root.appendChild(frag)
		return this.root
	}

	update(url) {
		this.urldisplay.textContent = url
		var range = document.createRange();
		range.selectNode(this.urldisplay);
		window.getSelection().removeAllRanges();
		window.getSelection().addRange(range);
	}
}

export default UrlDisplay
