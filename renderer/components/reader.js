import { addClass, removeClass, listen } from '../dom.js'
import Dictionary from '../dictionary.js'

const { ipcRenderer, webContents } = require('electron')
import Settings from './settings.js'
import Definition from './definition.js'
import Bookmark from './bookmark.js'
import Bookmarks from './bookmarks.js'
import UrlDisplay from './showurl.js'

let current, webview, dictionary, settings, selection, selectionObj,
definition, definitionMenu, bookmark, bookmarkMenu,
bookmarksMenu, bookmarks, urlMenu, urldisplay
let scrollto = -1

const menuElements = {
	library: document.querySelector('#menu-library'),
	libTitle: document.querySelector('.lib-title')
}

ipcRenderer.on('tagged-bookmark-set', (event, bm) => {
	scrollto = bm.scroll
	if (webview.src !== bm.url) {
		webview.src = bm.url
	} else handleScroll()
})

const settingsButton = document.querySelector('#read .menu-settings')
const bookmarksButton = document.querySelector('#read .menu-bookmarks')
const furthestButton = document.querySelector('#read .menu-furthest')
const homeButton = document.querySelector('#read .menu-home')

ipcRenderer.on("send-library", (e, lib) => {
	const wv = document.createElement('webview')
	wv.setAttribute('src', 'about:blank')
	wv.setAttribute('preload', lib.preload)
	document.querySelector('#webview-container').appendChild(wv)
	webview = wv
	webview.addEventListener('dom-ready', handleScroll)
	webview.addEventListener('ipc-message', handleMessage)
	webview.addEventListener('did-navigate', didNavigate)
	webview.addEventListener('did-navigate-in-page', didNavigate)

	const settingsMenu = document.querySelector('#read .settings-container')

	settings = new Settings(settingsMenu, 'book')

	listen(settingsButton, 'click', e => {
		if (current && current.lang) {
			settings.update({lang: current.lang})
		}
		settings.toggle()
	})

	dictionary = new Dictionary(lib.settings)
	definitionMenu = document.querySelector('#read .definition-container')
	definition = new Definition(definitionMenu)

	bookmarkMenu = document.querySelector('#read .bookmark-container')
	bookmark = new Bookmark(bookmarkMenu)

	urlMenu = document.querySelector('#read .urldisplay-container')
	urldisplay = new UrlDisplay(urlMenu)
	listen(menuElements.libTitle, 'click', () => {
		console.log('click', webview.src)
		urldisplay.update(webview.src)
		urldisplay.toggle()
	})


	bookmarksMenu = document.querySelector('#read .bookmarks-container')
	bookmarks = new Bookmarks(bookmarksMenu, goBookmark)
	listen(bookmarksButton, 'click', () => toggleBookmarks())

	listen(furthestButton, 'click', e => {
		const urlObj = current.urls[current.urls.length - 1]
		current.urlindex = urlObj.id
		ipcRenderer.send("update-book-url-index", current.urlindex);

		scrollto = urlObj.maxscroll
		if (webview.src !== urlObj.url) {
			webview.src = urlObj.url
		} else handleScroll()
	})

	function toggleBookmarks() {
		bookmarks.update(current.bookmarks)
		bookmarks.toggle()
	}

	listen(homeButton, 'click', () => {
		const urlObj = current.urls[0]
		current.urlindex = urlObj.id
		ipcRenderer.send("update-book-url-index", current.urlindex);

		scrollto = 0
		if (webview.src !== urlObj.url) {
			 webview.src = urlObj.url
		} else handleScroll()
	})
})

function enableButtons(enable) {
	const fn = enable ? removeClass : addClass
	fn(homeButton, 'no-click')
	fn(bookmarksButton, 'no-click')
	fn(furthestButton, 'no-click')
	fn(settingsButton, 'no-click')
}

ipcRenderer.on('current-book-set', (e, book) => {
	if (book.id > -1) {
		menuElements.libTitle.textContent = book.title
		const src = book.urls.find(u => u.id === book.urlindex).url
		if (webview.src !== src)
			webview.setAttribute('src', src)
		current = book
		enableButtons(true)
	}
	if (book.id === -2) {
		current = book
		enableButtons(false)
		menuElements.libTitle.textContent = `view only - ${book.title}`
	}
})

listen(menuElements.library, 'click', () => {
	ipcRenderer.send('current-book', { id: -1, collection: -1 })
})

function handleScroll(event, max) {
	if (!current) return
	if(scrollto === -1) {
		const urlObject = current.urls.find(item => item.id === current.urlindex)
		ipcRenderer.send('scrollto', urlObject.scroll)
	} else {
		ipcRenderer.send('scrollto', scrollto)
	}
	const highlights = current.bookmarks
	.filter(b => (b.url_id === current.urlindex))
	webview.send('setbookmarks', highlights)
	scrollto = -1
}

function didNavigate(event) {
	if (event.url === "about:blank" || !current || current.id === -2) return
	let urlObject = current.urls.find(item => item.url === event.url)
	if (urlObject) {
		ipcRenderer.send("update-book-url-index", urlObject.urlindex);
	} else {
		ipcRenderer.send("update-book-url", { book: current.id, url: event.url });
	}

}

function handleMessage(e) {
	if (e.channel === 'word-selected') {
		const word = e.args[0]
		const lang = settings.lang
		if (word) dictionary.getDefinitions(word, lang).then(definitions => {
			definition.update(definitions)
			definition.show()
		})
	} else if (e.channel === 'phrase-selected' && current && current.id > 0) {
		selection = e.args[0]
		selectionObj = e.args[1]
		bookmark.update(selection, selectionObj)
		bookmark.show()
	} else if (e.channel === 'preload-log') {
		console.log("********** preload log ***********\n", ...e.args, "\n************ end log *************")
	}
}

function  goBookmark(bookmark) {
	const urlindex = current.urls.find(u => u.url === bookmark.url).id
	ipcRenderer.send('update-book-url-index', urlindex)

	current.urlindex = urlindex
	scrollto = bookmark.scroll
	if (webview.src !== bookmark.url) {
		webview.src = bookmark.url
	} else handleScroll()

	bookmarks.hide()
}

