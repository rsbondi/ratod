import { el, listen } from '../dom.js'
import Collapsable from './base/collapsable.js'

class Definition extends Collapsable {
	constructor(container) {
		super(container)
	}

	mount() {
		this.root = el('definition-content')
		let frag = document.createDocumentFragment()

		this.h3 = el('', 'h3')
		frag.appendChild(this.h3)

		const closex = el('close')
		closex.textContent = 'x'
		listen(closex, 'click', this.hide.bind(this))
		frag.appendChild(closex)

		this.ul = el('', 'ul')
		frag.appendChild(this.ul)

		this.root.appendChild(frag)
		return this.root
	}

	update(defs) {
		this.ul.innerHTML = ''
		defs.forEach(meaning => {
			const li = el('', 'li')
			const em = el('', 'em')
			em.textContent = meaning.partOfSpeech
			li.appendChild(em)
			meaning.definitions.forEach(def => {
				const div = el()
				div.textContent = `-- ${def.definition}`
				li.appendChild(div)
			})
			this.ul.append(li)
		})
	}
}

export default Definition
