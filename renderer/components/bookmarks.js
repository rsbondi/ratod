import { el, listen } from '../dom.js'
import Collapsable from './base/collapsable.js'
const { ipcRenderer } = require('electron')

class Bookmarks extends Collapsable{
	constructor(container, nav) {
		super(container)
		this.nav = nav
		this. disposables = []

		ipcRenderer.on('bookmark-added', this.hide.bind(this))
	}

	dispose() {
		this.disposables.forEach(d => d())
		this.disposables = []
	}

	mount() {
		this.root = el('bookmarks-content')
		let frag = document.createDocumentFragment()

		this.h3 = el('', 'h3')
		this.h3.textContent = 'Bookmarks'
		frag.appendChild(this.h3)

		const closex = el('close')
		closex.textContent = 'x'
		listen(closex, 'click', () => {
			this.hide()
			this.dispose()
		})
		frag.appendChild(closex)

		const none = el('none collapsed')
		none.textContent = 'No bookmarks, please select text and open again to add.'
		frag.appendChild(none)

		this.ul = el('', 'ul')
		frag.appendChild(this.ul)

		this.root.appendChild(frag)
		return this.root
	}

	update(bookmarks) {
		this.dispose()
		this.ul.innerHTML = ''
		bookmarks.forEach(bookmark => {
			const li = el('', 'li')
			this.ul.appendChild(li)
			const content = el('bm-content')
			li.appendChild(content)
			this.disposables.push(
				listen(content, 'click', () => this.nav(bookmark))
			)

			const phrasegrp = el()
			content.appendChild(phrasegrp)
			const h5ph = el('', 'h5')
			h5ph.textContent = 'text'
			phrasegrp.appendChild(h5ph)
			const phrase = el()
			phrase.textContent = bookmark.phrase
			phrasegrp.appendChild(phrase)

			if (bookmark.note) {
				const notegrp = el()
				content.appendChild(notegrp)
				const h5note = el('', 'h5')
				h5note.textContent = 'note'
				notegrp.appendChild(h5note)
				const note = el()
				note.textContent = bookmark.note
				notegrp.appendChild(note)
			}
		})
	}
}

export default Bookmarks

