import { el, listen, addClass, removeClass } from '../dom.js'

class PotentialTags {
	constructor(selected) {
		this.tags = []
		this.disposables = []
		this.selected = selected
	}


	mount() {
		this.root = el('tags-select collapsed')
		let frag = document.createDocumentFragment()

		this.tagsList = el('tag-list', 'ul')
		frag.appendChild(this.tagsList)

		this.root.appendChild(frag)

		return this.root
	}

	update(tags) {
		this.dispose()
		this.tags = tags
		removeClass(this.root, 'collapsed')
		tags.forEach(tag => {
			const li = el('', 'li')
			li.textContent = tag.label
			this.disposables.push(listen(li, 'click', () => {
				this.selected(tag)
				this.dispose()
			}))
			this.tagsList.appendChild(li)
		})
	}

	dispose() {
		this.disposables.forEach(d => d())
		this.disposables = []
		for (var t=this.tagsList.childNodes.length-1; t>=0; t--)
			this.tagsList.removeChild(this.tagsList.childNodes[t])
		addClass(this.root, 'collapsed')
	}
}

export default PotentialTags
