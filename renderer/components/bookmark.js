import { el, listen } from '../dom.js'
const { ipcRenderer } = require('electron')
import TagsInput from './tagsinput.js'
import Collapsable from './base/collapsable.js'

class Bookmark extends Collapsable {
	constructor(container) {
		super(container)
		ipcRenderer.on('bookmark-added', () => {
			this.hide()
			this.tagsinput.dispose()
			this.textarea.value = ''
		})
	}

	mount() {
		this.root = el('bookmark-content')
		let frag = document.createDocumentFragment()

		this.h3 = el('', 'h3')
		this.h3.textContent = 'Bookmark'
		frag.appendChild(this.h3)

		const closex = el('close')
		closex.textContent = 'x'
		listen(closex, 'click', () => {
			this.hide()
			this.tagsinput.dispose()
			this.textarea.value = ''
		})
		frag.appendChild(closex)

		this.phrase = el('phrase')
		frag.appendChild(this.phrase)

		const noteContainer = el()
		const h4 = el('', 'h4')
		h4.textContent = "Note:"
		noteContainer.appendChild(h4)

		this.textarea = el('', 'textarea')
		this.textarea.setAttribute('rows', 5)
		noteContainer.appendChild(this.textarea)
		frag.appendChild(noteContainer)

		this.tagsinput = new TagsInput()
		frag.appendChild(this.tagsinput.mount())

		const buttonContainer = el()
		const button = el('', 'button')
		button.textContent = 'Add'
		listen(button, 'click', e => {
			ipcRenderer.send('add-bookmark', {
				phrase: this.phrase.textContent,
        note: this.textarea.value,
				tags: this.tagsinput.getTags(),
				selection: JSON.stringify(this.selection)
      })
		})
		buttonContainer.appendChild(button)

		frag.appendChild(buttonContainer)
		this.root.appendChild(frag)
		return this.root
	}

	update(phrase, selection) {
		this.phrase.textContent = phrase
		this.selection = selection
	}
}

export default Bookmark
