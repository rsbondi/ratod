import { el, addClass, removeClass, listen } from '../dom.js'
const { ipcRenderer } = require('electron')

class CollectionList {
	constructor(collection) {
		this.collection = collection
		this.showCollection = collection.expanded
		this.visible = !!collection.books.length
		this.books = {}

		ipcRenderer.on('collection-expand', (e, arg) => {
			if (this.collection.id !== arg.id) return;
			if (arg.expanded) {
				removeClass(this.ul, 'collapsed')
			} else {
				addClass(this.ul, 'collapsed')
			}
		})

		ipcRenderer.on('book-removed', (e, arg) => {
			const li = this.ul.querySelector(`li[data-book="${arg}"]`)
			if (!li) return; // different collection
			this.ul.removeChild(li)
			this.books[arg].dispose()
			delete (this.books[arg])
			if (!this.ul.children.length) addClass(this.root, 'collapsed')
		})
	}

	add(book) {
		removeClass(this.root, 'collapsed')
		this.mountBook(book)
	}

	removeBook(book) {
		ipcRenderer.send('remove-book', book.id)
	}

	mountBook(book) {
		const li = document.createElement('li')
		li.setAttribute('data-book', book.id)
		const divbook = el('book')
		li.appendChild(divbook)
		const divtitle = el('title')
		divtitle.textContent = book.title
		book.dispose = listen(divtitle, 'click', e => {
			this.currentBook = book
			this.selectBook(book)
		})
		this.books[book.id] = book

		divbook.appendChild(divtitle)

		const removeButton = el('trash', 'button')
		removeButton.textContent = 'x'
		listen(removeButton, 'click', () => this.removeBook(book))

		divbook.appendChild(removeButton)

		this.ul.appendChild(li)
	}

	mount() {
		let root = el()
		root.setAttribute('data-collection', this.collection.id)
		let frag = document.createDocumentFragment()
		this.root = root
		this.currentBook = -1

		if (!this.collection.books.length) {
			addClass(this.root, 'collapsed')
		}

		const h5 = el('collection-title', 'h5')
		h5.textContent = this.collection.title
		listen(h5, 'click', this.toggleCollection.bind(this))
		frag.appendChild(h5)

		this.ul = el('', 'ul')
		this.collection.books.forEach(this.mountBook.bind(this))
		frag.appendChild(this.ul)
		if (!this.collection.expanded) addClass(this.ul, 'collapsed')
		root.appendChild(frag)
		return root
	}

	toggleCollection() {
		this.showCollection = !this.showCollection
		ipcRenderer.send("set-collection-expanded", { id: this.collection.id, expanded: this.showCollection })
	}

	selectBook(book) {
		ipcRenderer.send('current-book', book)
	}

}

export default CollectionList
