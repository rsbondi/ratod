import { el, listen } from '../dom.js'
import AddBookForm from './addbookform.js'
const { ipcRenderer } = require('electron')
import Collapsable from './base/collapsable.js'

const languages = {
	en: 'English',
	hi: 'Hindi',
	es: 'Spanish',
	fr: 'French',
	ja: 'Japanese',
	ru: 'Russian',
	de: 'German',
	it: 'Italian',
	ko: 'Korean',
	'pt-BR': 'Brazilian Portuguese',
	ar: 'Arabic',
	tr: 'Turkish',
}

class Settings extends Collapsable {
	constructor(container, mode) {
		super(container, true)
		this.mode = mode
		this.setContainer()
	}

	mount() {
		let root = el('settings')
		let frag = document.createDocumentFragment()
		this.root = root
		const closex = el('close')
		closex.textContent = 'x'
		listen(closex, 'click', this.hide.bind(this))
		frag.appendChild(closex)
		const settingHeading = el('', 'h3')
		settingHeading.textContent = 'Settings'
		frag.appendChild(settingHeading)
		const div = el()
		const label = el('label')
		label.textContent = 'language'
		this.select = el('', 'select')
		div.appendChild(label)
		div.appendChild(this.select)
		Object.keys(languages).forEach(lang => {
			const option = el('', 'option')
			option.setAttribute('value', lang)
			option.textContent = languages[lang]
			this.select.appendChild(option)
		})
		const saveLangButton = el('save-language', 'button')
		saveLangButton.textContent = 'Save language'
		listen(saveLangButton, 'click', this.saveLang.bind(this))
		div.appendChild(saveLangButton)

		frag.appendChild(div)

		if (this.mode === 'global') {
			const libHeading = el('lib-manage', 'h3')
			libHeading.textContent = 'Library Management'
			frag.appendChild(libHeading)
			const collectionDiv = el('item')
			const collectionLabel = el('label')
			collectionLabel.textContent = 'add collection'
			collectionDiv.appendChild(collectionLabel)
			this.collectionInput = el('', 'input')
			this.collectionInput.setAttribute('type', 'text')
			this.collectionInput.setAttribute('placeholder', 'add collection')
			collectionDiv.appendChild(this.collectionInput)
			const button = el('add-collection', 'button')
			button.textContent = 'Add collection'
			listen(button, 'click', this.addCollection.bind(this))
			collectionDiv.appendChild(button)

			frag.appendChild(collectionDiv)

			const addbook = new AddBookForm()
			frag.appendChild(addbook.mount())
		}

		root.appendChild(frag)
		return root
	}

	update(newSettings) {
		if (newSettings.lang) {
			this.select.value = newSettings.lang
		}
	}

	addCollection(e) {
		ipcRenderer.send("add-collection", this.collectionInput.value)
		this.collectionInput.value = ""
	}

	saveLang(e) {
		const settings = { mode: this.mode, lang: this.select.value }
		ipcRenderer.send("save-lang", settings)
		if (this.mode === 'book') this.close()
	}

}

export default Settings
