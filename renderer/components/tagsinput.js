import { el, listen } from '../dom.js'
const { ipcRenderer } = require('electron')
import TagItem from './tagitem.js'
import PotentialTags from './potentialtags.js'

class TagsInput {
	constructor() {
		this.availableTags = []
		this.disposables = []

		ipcRenderer.on('tags-added', this.tagAdded.bind(this))
		ipcRenderer.on("send-tags", this.tagsReceived.bind(this))
		ipcRenderer.send('get-tags')
	}

	mount() {
		this.root = el('tags-content')
		let frag = document.createDocumentFragment()

		const h4t = el('', 'h4')
		h4t.textContent = "Tags:"
		frag.appendChild(h4t)

		this.tagsInput = el('tags', 'input')
		this.tagsInput.setAttribute('type', 'text')
		listen(this.tagsInput, 'keyup', this.keyup.bind(this))
		frag.appendChild(this.tagsInput)

		this.tagsList = el('tag-list', 'ul')
		frag.appendChild(this.tagsList)

		this.potentialtags = new PotentialTags(this.addTag.bind(this))
		frag.appendChild(this.potentialtags.mount())

		this.root.appendChild(frag)

		return this.root
	}

	keyup(e) {
		if(e.which === 13) {
			let tag = this.availableTags.find(t => t.label === this.tagsInput.value)
			if (!tag) tag = {id: -1, label: this.tagsInput.value}
			this.addTag(tag)
			this.tagsInput.value = ''
			return
		}
		if(this.tagsInput.value.length > 1) {
			const potentials = this.availableTags
			.filter(t => t.label.indexOf(this.tagsInput.value) > -1)
			.filter(t => this.getTags().map(t => t.label) .indexOf(t.label) === -1)
			.sort((a, b) => a.label.indexOf(this.tagsInput.value) - b.label.indexOf(this.tagsInput.value))

			if (potentials.length) {
				this.potentialtags.update(potentials)
			} else this.potentialtags.dispose()
		} else this.potentialtags.dispose()
	}

	tagAdded(e, tags) {
		this.availableTags.push(...tags)
	}

	getTags() {
		return this.disposables.map(d => d.tag)
	}

	addTag(tag) {
		let tagitem
		const tagItemInstance = new TagItem(tag, () => {
			this.tagsList.removeChild(tagitem)
			this.disposables.splice(this.disposables.indexOf(tagItemInstance), 1)
		})
		this.disposables.push(tagItemInstance)
		tagitem = tagItemInstance.mount()
		this.tagsList.appendChild(tagitem)
		this.tagsInput.value = ''
	}

	dispose() {
		this.disposables.forEach(d => d.dispose())
		this.disposables = []
		for (var t=this.tagsList.childNodes.length-1; t>=0; t--)
			this.tagsList.removeChild(this.tagsList.childNodes[t])
		this.potentialtags.dispose()
		this.tagsInput.value = ''
	}

	tagsReceived(e, tags) {
		this.availableTags = tags
	}
}

export default TagsInput
