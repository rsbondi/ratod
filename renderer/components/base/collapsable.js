import { addClass, removeClass } from '../../dom.js'

class Collapsable {
	constructor(container, skipMount) {
		this.visible = false
		this.skipMount = skipMount
		this.container = container
		if (this.container && !skipMount) {
			this.container.appendChild(this.mount())
			this.hide()
		}
	}

	setContainer(container) {
		if (container) this.container = container
		if (this.skipMount) {
			this.container.appendChild(this.mount())
			this.hide()
		}
	}

	show() {
		this.visible = true
		removeClass(this.container, Collapsable.collapseClass)
	}

	hide() {
		this.visible = false
		addClass(this.container, Collapsable.collapseClass)
	}

	toggle() {
		this.visible = !this.visible
		if (this.visible) {
			this.show()
		} else {
			this.hide()
		}
	}
}

Collapsable.collapseClass = 'collapsed'

export default Collapsable
