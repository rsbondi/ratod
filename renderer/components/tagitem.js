import { el, listen } from '../dom.js'

class TagItem {
	constructor(tag, removeItem) {
		this.tag = tag
		this.removeItem = removeItem
	}

	mount() {
		this.root = el('tag-item', 'li')
		let frag = document.createDocumentFragment()

		const box = el('box')
		frag.appendChild(box)

		const label = el('label')
		label.textContent = this.tag.label
		box.appendChild(label)

		const remove = el('remove')
		remove.textContent = 'x'
		this.dispose = listen(remove, 'click', () => {
			this.dispose()
			this.removeItem()
		})
		box.appendChild(remove)

		this.root.appendChild(frag)
		return this.root
	}

}

export default TagItem
