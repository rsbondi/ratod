import { el, addClass, removeClass, listen } from '../dom.js'
const { ipcRenderer } = require('electron')

class AddBookForm {
	constructor() {

		ipcRenderer.on('collection-added', (e, col) => {
			const option = el('', 'option')
			option.setAttribute('value', col.id)
			option.textContent = col.title
			this.selectAddToCollection.appendChild(option)
		})

		ipcRenderer.on("send-library", (e, lib) => {
			lib.collections.forEach(collection => {
				const option = el('', 'option')
				option.setAttribute('value', collection.id)
				option.textContent = collection.title
				this.selectAddToCollection.appendChild(option)
			})
		})
	}

	mount() {
		this.root = el('book-form')
		let frag = document.createDocumentFragment()

		const bookLabel = el('', 'h4')
		bookLabel.textContent = 'add source'
		frag.appendChild(bookLabel)


		const titleInput = el('', 'input')
		titleInput.setAttribute('type', 'text')
		titleInput.setAttribute('placeholder', 'title')
		frag.appendChild(titleInput)

		const urlInput = el('', 'input')
		urlInput.setAttribute('type', 'text')
		urlInput.setAttribute('placeholder', 'url')
		frag.appendChild(urlInput)

		const label = el()
		label.textContent = 'add to collection'
		frag.appendChild(label)

		this.selectAddToCollection = el('', 'select')
		frag.appendChild(this.selectAddToCollection)

		const button = el('', 'button')
		button.textContent = 'Add source'
		listen(button, 'click', e => {
			ipcRenderer.send("add-book", {
				title: titleInput.value,
				url: urlInput.value,
				collection: this.selectAddToCollection.value
			})
			titleInput.value = ""
			urlInput.value = ""
		})
		frag.appendChild(button)

		this.root.appendChild(frag)
		return this.root
	}
}

export default AddBookForm

