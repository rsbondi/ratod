const { ipcRenderer } = require('electron')
import CollectionList from './collectionlist.js'
import Settings from './settings.js'
import { addClass, removeClass, listen } from '../dom.js'
import PotentialTags from './potentialtags.js'
import Bookmarks from './bookmarks.js'


const settingsMenu = document.querySelector('#lib .settings-container')
const settingsButton = document.querySelector('#lib .menu-settings')
const tagsMenu = document.querySelector('#lib .tags-container')
const tagsButton = document.querySelector('#lib .menu-tags')
const bookmarksMenu = document.querySelector('#lib .bookmarks-container')
const searchInput = document.querySelector('#search-input')

let liblist
const collectionLists = {}
let globalSettings = {}
let showTags = false
const availableTags = []

ipcRenderer.on('collection-added', (e, col) => {
	const item = new CollectionList(col)
	collectionLists[col.id] = item
	liblist.appendChild(item.mount())
})

ipcRenderer.on('book-added', (e, book) => {
	collectionLists[book.collection].add(book)
})

ipcRenderer.on("send-library", (e, lib) => {
	const collections = collectionLib(lib)
	globalSettings = lib.settings
	let frag = document.createDocumentFragment()

	collections.forEach(collection => {
		const item = new CollectionList(collection)
		collectionLists[collection.id] = item
		frag.appendChild(item.mount())
	})
	liblist = document.querySelector('#liblist')
	liblist.appendChild(frag)
})

ipcRenderer.on('tags-added', (e, tags) => {
	availableTags.push(...tags)
	availableTags.sort((a, b) => a.label > b.label ? 1 : -1)
})

const settings = new Settings(settingsMenu,'global')
listen(settingsButton, 'click', e => {
	settings.update({lang: globalSettings.lang})
	settings.toggle()
})

const tags = new PotentialTags(tagSelected)
tagsMenu.appendChild(tags.mount())
listen(tagsButton, 'click', e => {
	toggleTags()
})

listen(searchInput, 'keyup', e=> {
	if (e.which === 13) {
		ipcRenderer.send('search-bookmarks', searchInput.value)
		searchInput.value = ''
	}
})

ipcRenderer.on("send-tags", tagsReceived)

function tagsReceived(e, allTags) {
	availableTags.push(...allTags)
}

const bookmarks = new Bookmarks(bookmarksMenu, goBookmark)

ipcRenderer.on("tagged-bookmarks", bookmarksReceived)
ipcRenderer.on("bookmark-search-result", bookmarksReceived)

function bookmarksReceived(e, bms) {
 bookmarks.update(bms)
 bookmarks.toggle()
}

function  goBookmark(bookmark) {
	ipcRenderer.send('current-book', {id: -2, title: bookmark.title, bookmarks: [bookmark]})
	ipcRenderer.send('tagged-bookmark', bookmark)
	bookmarks.toggle()
}

function tagSelected(tag) {
	ipcRenderer.send('get-tag-bookmarks', tag.id)
	toggleTags()
}

function toggleTags() {
	showTags = !showTags
	if (showTags) {
		tags.update(availableTags.filter(t => t.bookmark_count))
		removeClass(tagsMenu, 'collapsed')
	} else addClass(tagsMenu, 'collapsed')
}

ipcRenderer.send("get-library");

function collectionLib(state) {
	const collectionLib = state.collections.reduce((lib, col) => {
		lib.push({ ...col, books: [] })
		return lib
	}, [])
	state.books.forEach(book => {
		if (!book.collection) {
			book.collection = 1
		}
		const col = collectionLib.find(c => c.id === book.collection)
		col && col.books.push(book)
	})
	return collectionLib
}
export default {}
