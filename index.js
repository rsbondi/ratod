'use strict';
const path = require('path');
const {app, BrowserWindow, Menu} = require('electron');
const {is} = require('electron-util');
const debug = require('electron-debug');
require('./main/ipchandler')

debug();

app.setAppUserModelId("net.richardbondi.ratod");

// Prevent window from being garbage collected
let mainWindow;

const createMainWindow = async () => {
	const win = new BrowserWindow({
		title: app.name,
		show: false,
		width: 800,
		height: 800,
		webPreferences: {
			nodeIntegration: true,
			webviewTag: true
		}
	});
	win.setMenu(null)

	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		// Dereference the window
		// For multiple windows store them in an array
		mainWindow = undefined;
	});

	await win.loadFile(path.join(__dirname, 'renderer', 'index.html'));

	return win;
};

// Prevent multiple instances of the app
if (!app.requestSingleInstanceLock()) {
	app.quit();
}

app.on('second-instance', () => {
	if (mainWindow) {
		if (mainWindow.isMinimized()) {
			mainWindow.restore();
		}

		mainWindow.show();
	}
});

app.on('window-all-closed', () => {
	if (!is.macos) {
		app.quit();
	}
});

app.on('activate', () => {
	if (!mainWindow) {
		mainWindow = createMainWindow();
	}
});

(async () => {
	await app.whenReady();
	mainWindow = await createMainWindow();

	// mainWindow.webContents.executeJavaScript(`document.querySelector('header p').textContent = 'no ui yet'`);
})();
