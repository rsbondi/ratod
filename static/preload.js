const { ipcRenderer } = require('electron')
let scrollTimer, selectTimer

document.addEventListener('scroll', e => {
	clearTimeout(scrollTimer)
	scrollTimer = setTimeout(() => {
		ipcRenderer.send('scroll', window.scrollY)
	}, 1000)
})

let key = 0
let y = 0

document.addEventListener('selectionchange', e => {
	clearTimeout(selectTimer)
	selectTimer = setTimeout(() => {
		const selection = document.getSelection().toString();
		if (selection.split(/\s/).length === 1)
			ipcRenderer.sendToHost('word-selected', selection)
		if (selection.split(/\s/).length > 1) {
			const range = document.getSelection().getRangeAt(0)
			ipcRenderer.sendToHost('phrase-selected', selection, rangeToObj(range))
		}
	}, 1000)
})

ipcRenderer.on('setscroll', (event, arg) => {
	y = arg
	window.scroll(0, arg)
})

ipcRenderer.on('bookmark-added', (event, args) => {
	const range = document.getSelection().getRangeAt(0)
	highlightRange(range, args.note)
	document.getSelection().removeAllRanges()
})


ipcRenderer.on('setbookmarks', (event, args) => {
	args.forEach(h => {
		try {
			document.getSelection().removeAllRanges()
			const lines = h.phrase.split("\n").filter(f => f)
			lines.forEach(line => {
				while(window.find(line.trim())) {
					const range = document.getSelection().getRangeAt(0)
					highlightRange(range, h.note)
				}
			})
		} catch (e) { log(e.message) }
	})
	document.getSelection().removeAllRanges()
	window.scroll(0, y)
})

ipcRenderer.send('webview-loaded')

function highlightElement(node, note) {
	const span = document.createElement("span");
	span.style.backgroundColor = "antiquewhite";
	if (note) span.setAttribute('title', note)
	node.parentNode.replaceChild(span, node);
	span.appendChild(node);
  [...(node.childNodes)].forEach(n => highlightElement(n, note))
}

function highlightRange(range, note) {
	const selectionContents = range.extractContents()
  ;[...(selectionContents.childNodes)].forEach(node => {
    highlightElement(node, note)
  })
  range.insertNode(selectionContents)
}

// TODO: cleanup, remove this and 'highlight' from schema
function rangeToObj(range) {
	return {
		startKey: range.startContainer.parentNode.dataset.key || 0,
		startTextIndex: Array.prototype.indexOf.call(range.startContainer.parentNode.childNodes, range.startContainer),
		endKey: range.endContainer.parentNode.dataset.key || 0,
		endTextIndex: Array.prototype.indexOf.call(range.endContainer.parentNode.childNodes, range.endContainer),
		startOffset: range.startOffset,
		endOffset: range.endOffset
	}
}

function log(...message) {
	ipcRenderer.sendToHost('preload-log', ...message)
}



