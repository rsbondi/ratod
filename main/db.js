const sqlite3 = require('sqlite3').verbose()
const path = require('path')
const fs = require('fs')
const os = require('os')

const dbfile = process.env.SQLITE_DATABASE || path.join(os.homedir(), '.hbook.db')
const DB_LANG_TYPE = {
	GLOBAL: 0,
	COLLECTION: 1,
	SOURCE: 2
}

const bookQuery = `
SELECT * FROM (
	SELECT
		c.id collection_id, c.title collection_title,
		s.id source_id, s.title source_title, s.urlindex, l.label lang,
		u.id url_id, u.url, u.scroll, u.maxscroll,
		NULL bookmark_id, NULL bookmark_url, NULL bookmark_url_id, NULL phrase, NULL note, NULL bookmark_scroll, NULL highlight
	FROM  collection c
	JOIN source s ON s.collection_id=c.id
	JOIN url u on u.source_id=s.id
  LEFT JOIN language l ON l.source_id = s.id

	UNION
	SELECT
		c.id collection_id, c.title collection_title,
		s.id source_id, s.title source_title, s.urlindex, l.label lang,
		NULL url_id, NULL url, NULL scroll, NULL maxscroll,
		b.id bookmark_id, u.url bookmark_url, b.url_id bookmark_url_id, b.phrase, b.note, b.scroll bookmark_scroll, b.highlight
	FROM  collection c
	JOIN source s ON s.collection_id=c.id
	JOIN bookmark b ON b.source_id=s.id
	JOIN url u on b.url_id=u.id
  LEFT JOIN language l ON l.source_id = s.id
)
`

class DataBase {
	constructor() {
		this.db = new sqlite3.Database(dbfile);
	}

	create() {
		return new Promise(async (resolve, reject) => {
			const table = await this.queryAsync("SELECT count(*) c FROM sqlite_master WHERE type='table' AND name='collection'")
			if (table[0].c) {
				resolve()
				return
			}
			const dataSql = fs.readFileSync(path.join(__dirname, `reader.sql`)).toString()
			const commands = dataSql.toString().split('\n\n')
				.filter(q => q.trim())
				.map(q => q.trim())

			try {
				for (var c = 0; c < commands.length; c++) {
					await this.runAsync(commands[c])
				}
			} catch (err) {
				this.runAsync('ROLLBACK')
					.then(() => reject(err))

				return
			}
			resolve()
		})
	}

	getGlobalLanguage() {
		return this.getLanguage(-1, DB_LANG_TYPE.GLOBAL)
	}

	getLanguage(sourceId, sourceType) {
		return new Promise(async (resolve, reject) => {
			try {
				const src = sourceId === 'NULL' ? ' IS NULL' : `=${sourceId}`
				const query = `SELECT * FROM language WHERE source_id=? AND source_type=?`
				const source = await this.queryAsync(query, sourceId, sourceType)
				resolve(source)
			} catch (err) {
				console.error(err.message)
				reject(err)
			}
		})
	}

	setGlobalLanguage(lang) {
		return this.setLanguage(-1, DB_LANG_TYPE.GLOBAL, lang)
	}

	setLanguage(sourceId, sourceType, lang) {
		return new Promise(async (resolve, reject) => {
			try {
				const source = await this.getLanguage(sourceId, sourceType)
				let query, params
				if (source.length) {
					query = `UPDATE language SET label=? WHERE source_type=? AND source_id=?`
					params = [lang, sourceType, sourceId]
				} else {
					query = `INSERT INTO language (source_id, source_type, label) VALUES (?, ?, ?)`
					params = [sourceId, sourceType, lang]
				}
				await this.runAsync(query, ...params)
				resolve()
			} catch (err) {
				console.error(err.message)
				reject(err)
			}
		})
	}

	setCollection({ title, expanded }, id) {
		return new Promise(async (resolve, reject) => {
			try {
				const sets = []
				const params = []
				let query
				if (title) {
					sets.push('title=?')
					params.push(title)
				}
				if (typeof expanded !== 'undefined') {
					sets.push('expanded=?')
					params.push(expanded)
				}
				if (id) {
					params.push(id)
					query = `UPDATE collection SET ${sets.join(', ')} WHERE id=?`
					console.log(sets, query)
				} else {
					query = `INSERT INTO collection (title) VALUES (?)`
				}
				const insert = await this.runAsync(query, ...params)
				resolve({ id: insert && insert.lastID || -1 })

			} catch (err) {
				reject(err)
			}
		})
	}

	addUrl(sourceId, url) {
		return new Promise(async (resolve, reject) => {
			try {
				const insert = await this.runAsync(`INSERT INTO url (source_id, url, scroll, maxscroll) VALUES(?, ?, 0, 0)`, sourceId, url)
				resolve({ id: insert && insert.lastID || -1 })
			} catch (err) {
				reject(err)
			}
		})
	}

	addBookmark(sourceId, phrase, note, urlId, scroll, tags, selection) {
		return new Promise(async (resolve, reject) => {
			try {
				const newTags = []
				await this.runAsync('BEGIN TRANSACTION;')
				const query = `INSERT INTO bookmark (source_id, phrase, note, url_id, scroll, highlight) VALUES(?, ?, ?, ?, ?, ?)`
				const insert = await this.runAsync(query, sourceId, phrase, note, urlId, scroll, selection)
				await Promise.all(tags.map(async t => {
					if (t.id === -1) {
						const taginsert = await this.runAsync(`INSERT INTO tag (label) VALUES(?)`, t.label)
						t.id = taginsert.lastID
						t.bookmark_count = 1
						newTags.push(t)
					}
					return this.runAsync(`INSERT INTO tag_ref (tag_id, bookmark_id) VALUES(?, ?)`, t.id, insert.lastID)
				}))
				await this.runAsync('COMMIT;')
				resolve({ id: insert && insert.lastID || -1, newTags })
			} catch (err) {
				await this.runAsync('ROLLBACK;')
				reject(err)
			}
		})
	}

	updateUrl(id, scroll, maxscroll) {
		return new Promise(async (resolve, reject) => {
			try {
				await this.runAsync(`UPDATE url SET scroll=?, maxscroll=COALESCE(?, 0) WHERE id=?`, scroll, maxscroll, id)
				resolve()
			} catch (err) {
				reject(err)
			}
		})
	}

	addSource(collection, title, url) {
		return new Promise(async (resolve, reject) => {
			try {
				await this.runAsync('BEGIN TRANSACTION;')
				const insertSource = await this.runAsync(`INSERT INTO source (collection_id, title) VALUES(?, ?)`, collection, title)
				const sourceId = insertSource.lastID
				const urlInsert = await this.addUrl(sourceId, url)
				const urlId = urlInsert.id
				await this.runAsync(`UPDATE source SET urlindex=? WHERE id=?`, urlId, sourceId)
				await this.runAsync('COMMIT;')
				resolve({ sourceId, urlId })
			} catch (err) {
				await this.runAsync('ROLLBACK;')
				reject(err)
			}
		})
	}

	removeSource(id) {
		return new Promise(async (resolve, reject) => {
			try {
				await this.runAsync('BEGIN TRANSACTION;')
				await this.runAsync(`DELETE FROM source WHERE id=?`, id)
				await this.runAsync(`DELETE FROM url WHERE source_id=?`, id)
				await this.runAsync('COMMIT;')
				resolve()
			} catch (err) {
				await this.runAsync('ROLLBACK;')
				reject(err)
			}
		})
	}

	updateSource(id, { urlindex, lang }) {
		return new Promise(async (resolve, reject) => {
			try {
				const params = []
				const sets = []
				if (urlindex) {
					sets.push('urlindex=?')
					params.push(urlindex)
				}
				if (lang) {
					sets.push('lang=?')
					params.push(lang)
				}
				params.push(id)
				await this.runAsync(`UPDATE source SET ${sets.join(', ')} WHERE id=?`, ...params)
				resolve()
			} catch (err) {
				reject(err)
			}
		})
	}

	getUrl(id) {
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(`SELECT * FROM url WHERE id=?;`, id)
				resolve(rows[0])
			} catch (err) {
				reject(err)
			}
		})
	}

	updateUrl(id, scroll, maxscroll) {
		const query = `UPDATE url SET scroll=?, maxscroll=? WHERE id=?`
		return new Promise(async (resolve, reject) => {
			try {
				await this.runAsync(query, scroll, maxscroll, id)
				resolve()
			} catch (err) {
				reject(err)
			}
		})
	}

	getBook(bookId) {
		const query = `
		${bookQuery}
		WHERE source_id=?
		ORDER BY url_id, bookmark_id
		;
		`
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(query, bookId)
				const json = rows.reduce((j, r) => {
					if (!j.source) {
						j.source = { id: r.source_id, title: r.source_title, urlindex: r.urlindex, urls: [], bookmarks: [], collection: r.collection_id, lang: r.lang }
					}
					if (r.url_id) {
						j.source.urls.push({ id: r.url_id, scroll: r.scroll, maxscroll: r.maxscroll, url: r.url })
					}
					if (r.bookmark_id) {
						j.source.bookmarks.push({
							id: r.bookmark_id, url_id: r.bookmark_url_id, url: r.bookmark_url,
							scroll: r.bookmark_scroll, note: r.note, phrase: r.phrase,
							highlight: r.highlight })
					}
					return j
				}, {
					source: null
				})
				resolve(json.source)
			} catch (err) {
				reject(err)
			}
		})
	}

	getCollection(id) {
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(`SELECT * FROM collection WHERE id=?`, id)
				resolve(rows[0])
			} catch (err) {
				reject(err)
			}
		})

	}

	getCollections() {
		const query =
			`
  ${bookQuery}
  ORDER BY collection_id, source_id, url_id, bookmark_id
  ;
  `
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(query)
				const json = rows.reduce((j, r) => {
					let source = j.library.find(s => s.id === r.source_id)
					if (!source) {
						source = { id: r.source_id, title: r.source_title, urlindex: r.urlindex, urls: [], bookmarks: [], collection: r.collection_id, lang: r.lang }
						j.library.push(source)
					}
					if (r.url_id) {
						source.urls.push({ id: r.url_id, scroll: r.scroll, maxscroll: r.maxscroll, url: r.url })
					}
					if (r.bookmark_id) {
						source.bookmarks.push({ id: r.bookmark_id, url_id: r.bookmark_url_id, url: r.bookmark_url, scroll: r.bookmark_scroll, note: r.note, phrase: r.phrase })
					}
					return j
				}, {
					library: [],
					settings: {},
					collections: [],
					tags: []
				})
				const cols = await this.queryAsync(`SELECT * FROM collection`)
				const lang = await this.queryAsync('SELECT label FROM language WHERE source_type=0')
				const tagQuery = `SELECT t.id, t.label, COUNT(r.tag_id) bookmark_count FROM tag t
				LEFT JOIN tag_ref r ON r.tag_id=t.id
				GROUP BY(r.tag_id)
				ORDER BY t.label
				;`
				const tags = await this.queryAsync(tagQuery)
				json.collections = cols
				json.settings = { lang: lang[0].label }
				json.tags = tags

				resolve(json)
			} catch (err) {
				reject(err)
			}
		})
	}

	searchBookmarks(text) {
		const query = `SELECT b.id, s.title, u.url, b.phrase, b.note, b.scroll, b.highlight
		FROM bookmark b
			JOIN source s ON s.id = b.source_id
			JOIN url u ON b.url_id = u.id
		WHERE b.id in (
		SELECT id FROM bookmark_fts WHERE bookmark_fts MATCH ? ORDER BY rank
		);
		`
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(query, text)
				resolve(rows)
			} catch (err) {
				reject(err)
			}
		})
	}

	getBookmarksForTag(id) {
		const query = `SELECT b.id, s.title, u.url, b.phrase, b.note, b.scroll, b.highlight,
		r.tag_id
		FROM bookmark b
    JOIN source s ON s.id = b.source_id
		JOIN tag_ref r ON r.bookmark_id = b.id
		JOIN tag t ON r.tag_id = t.id
		JOIN url u ON b.url_id = u.id
		WHERE t.id=?;
		`
		return new Promise(async (resolve, reject) => {
			try {
				const rows = await this.queryAsync(query, id)
				resolve(rows)
			} catch (err) {
				reject(err)
			}
		})
	}

	queryAsync(statement, ...params) {
		return new Promise(async (resolve, reject) => {
			this.db.all(statement, params, function (err, rows) {
				if (err) reject(err)
				else resolve(rows)
			})
		})
	}

	runAsync(statement, ...params) {
		return new Promise(async (resolve, reject) => {
			this.db.run(statement, params, function (err) {
				if (err) reject(err)
				else {
					resolve(this)
				}
			})
		})
	}

	close() {
		this.db.close((err) => {
			if (err) {
				return console.error(err.message);
			}
			console.log('Closed the database connection.');
		})
	}
}

module.exports = {
	db: new DataBase(),
	DB_LANG_TYPE
}
