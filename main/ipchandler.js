const ipc = require('electron').ipcMain
const path = require('path')
const { db, DB_LANG_TYPE } = require('./db')

let browserIPC, webcontentIPC

let books, settings, collections, tags, taggedBookmark

try {
	db.create().then(() => {
		db.getCollections().then(r => {
			books = r.library
			settings = r.settings
			collections = r.collections
			tags = r.tags
			console.log('books initialized', books)
		})
	})
} catch (err) {
	books = []
	console.error(err)
}

let currentBook

async function syncBooks(sender) {
	if (currentBook) {
		currentBook = await db.getBook(currentBook.id)
		sender.send('current-book-set', currentBook)
	}
}

ipc.on('add-book', async (event, args) => {
	const { title, collection, url } = args

	const src = await db.addSource(collection, title, url)
	const book = {
		title,
		collection,
		id: src.sourceId,
		urls: [
			{
				url: args.url,
				scroll: 0,
				id: src.urlId
			}
		],
		urlindex: src.urlId
	}
	event.sender.send('book-added', book)
	console.log('add-book', args)
	await syncBooks(event.sender)
})

ipc.on('add-collection', async (event, args) => {
	const col = await db.setCollection({ title: args })
	const newCollection = { id: col.id, title: args, books: [] }
	console.log('add-collection', JSON.stringify(newCollection))
	event.sender.send('collection-added', newCollection)
})

ipc.on('remove-book', async (event, args) => {
	await db.removeSource(args)
	event.sender.send('book-removed', args)
	await syncBooks(event.sender)
})

ipc.on('scroll', async (event, arg) => {
	if (!currentBook || currentBook.id < 0) return
	const url = await db.getUrl(currentBook.urlindex)
	console.log('ipc.on scroll', arg, Math.max(url.maxscroll, arg))
	await db.updateUrl(currentBook.urlindex, arg, Math.max(url.maxscroll, arg))
	browserIPC.send('scroll', arg)
	syncBooks(browserIPC)
})

ipc.on('get-library', (event, arg) => {
	browserIPC = event.sender
	event.sender.send('send-library', {
		books,
		settings,
		collections,
		tags,
		preload: 'file://' + path.resolve(path.join(__dirname, '..', 'static', 'preload.js'))
	})
})

ipc.on('get-tags', (event, arg) => {
	event.sender.send('send-tags', tags)
})

ipc.on('get-tag-bookmarks', async (event, arg) => {
	const bookmarks = await db.getBookmarksForTag(arg)
	event.sender.send ('tagged-bookmarks', bookmarks)
})

ipc.on('search-bookmarks', async (event, arg) => {
	const bookmarks = await db.searchBookmarks(arg)
	event.sender.send ('bookmark-search-result', bookmarks)
})

ipc.on('update-book-url', async (event, args) => {
	console.log('update-book', args)
	if (!currentBook || currentBook.id < 0) return
	const insert = await db.addUrl(args.book, args.url)
	await db.updateSource(args.book, { urlindex: insert.id })
	currentBook.urlindex = insert.id
	browserIPC.send('scroll', 0)
	if (webcontentIPC) {
		webcontentIPC.send('setscroll', 0)
	}
	await syncBooks(event.sender)
})

ipc.on('update-book-url-index', async (event, arg) => {
	console.log('update-book-url-index', arg)
	if (!arg || (!currentBook || currentBook.id < 0)) return
	await db.updateSource(currentBook.id, { urlindex: arg })
	await syncBooks(event.sender)
})

ipc.on('current-book', async (event, args) => {
	currentBook = await db.getBook(args.id)
	event.sender.send('current-book-set', args.id < 0 ? args : currentBook)
	console.log('current-book', args, currentBook)
})

ipc.on('tagged-bookmark', async (event, args) => {
	taggedBookmark = args
	browserIPC.send('tagged-bookmark-set', taggedBookmark)
	console.log('tagged-bookmark', args, currentBook)
})

ipc.on('add-bookmark', async (event, args) => {
	currentBook = await db.getBook(currentBook.id)
	const { phrase, note, tags, selection } = args
	const url = currentBook.urls.find(u => u.id === currentBook.urlindex)
	const insert = await db.addBookmark(currentBook.id, phrase, note, currentBook.urlindex, url.scroll, tags, selection)

	const bookmark = {
		id: insert.id,
		phrase,
		note,
		tags,
		selection,
		url: url.url,
		scroll: url.scroll
	}
	event.sender.send('bookmark-added', bookmark)
	webcontentIPC.send('bookmark-added', bookmark)
	if (insert.newTags.length) event.sender.send('tags-added', insert.newTags)
	console.log('add-bookmark', bookmark)
	await syncBooks(event.sender)
})

ipc.on('setscroll', async (event, arg) => {
	console.log('setscroll', arg)
	if (webcontentIPC) {
		const url = await db.getUrl(arg)
		webcontentIPC.send('setscroll', url.scroll)
	}
})

ipc.on('scrollto', (event, arg) => {
	console.log('scrollto', arg)
	webcontentIPC.send('setscroll', arg)
})

ipc.on('set-collection-expanded', async (event, arg) => {
	console.log('set-collection-expanded', arg)
	await db.setCollection({ expanded: arg.expanded }, arg.id)
	const collection = await db.getCollection(arg.id)
	event.sender.send('collection-expand', collection)
})

ipc.on('webview-loaded', (event, arg) => {
	webcontentIPC = event.sender
})

ipc.on('save-lang', async (event, arg) => {
	if (arg.mode === 'global') {
		await db.setGlobalLanguage(arg.lang)
		console.log('saved language', settings)
	} else {
		const book = currentBook.id
		await db.setLanguage(book, DB_LANG_TYPE.SOURCE, arg.lang)
		console.log('saved language', book)
	}
	await syncBooks(event.sender)
})
